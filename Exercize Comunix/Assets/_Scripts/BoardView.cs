﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView : MonoBehaviour
{
    [SerializeField]
    public GameObject cubePrefab;
    [SerializeField]
    public Transform boardStartingPoint;
    [HideInInspector]
    public GameObject[,,] BoardMadeOfPrefabs =
        new GameObject[Board.matDimX, Board.matDimY, Board.matDimZ];

    public float GameSpeed { get; set; }
    public float GameSpeedFactor { get; set; }

    public float score;
    public float rowsCompleted;

    private Board m_Board;

    public enum InputOptions
    {
        Up,
        Down,
        Left,
        Right,
        Rotate
    }

    private GameObject m_CubesParent;


    public void CreateBoard()
    {
        Vector3 pos = boardStartingPoint.position;
        m_Board = new Board();
        m_Board.ResetBoard();

        if (m_CubesParent != null)
            DestroyImmediate(m_CubesParent);

        m_CubesParent = new GameObject("Cubes Parent");
        m_CubesParent.transform.parent = this.transform;

        for (int x = 0; x < Board.matDimX; x++)
        {
            for (int z = 0; z < Board.matDimZ; z++)
            {
                for (int y = 0; y < Board.matDimY; y++)
                {
                    GameObject cube = 
                    Instantiate
                        (cubePrefab,
                        new Vector3(x + pos.x, y + pos.y, z + pos.z),
                        Quaternion.identity, 
                        this.transform);
                    cube.name = 
                        x.ToString() + " " + 
                        y.ToString() + " " + 
                        z.ToString();
                    BoardMadeOfPrefabs[x, y, z] = cube;
                    cube.transform.parent = m_CubesParent.transform;
                    cube.SetActive(false);
                }
            }
        }
        Debug.Log("Board Creation Completed");
    }

    public void StartGame()
    {
        score = 0;
        rowsCompleted = 0;
        StartCoroutine(TimeTick());
    }

    public void AddShape()
    {
        m_Board.AddShapeToTopOfShapeMatrixAtPointerPosition();
    }

    public void UpdateBoardViewAccordingToBoard()
    {
        m_Board.DoActionForEachCubeInMatrix(ShowCube, HideCube);
    }

    private void ShowCube(int x, int y, int z)
    {
        BoardMadeOfPrefabs[x, y, z].SetActive(true);
    }

    private void HideCube(int x, int y, int z)
    {
        BoardMadeOfPrefabs[x, y, z].SetActive(false);
    }

    bool isShapeLanded = false;

    private IEnumerator TimeTick()
    {
        
        isShapeLanded = m_Board.TurnTick();
        if (isShapeLanded)
        {
            m_Board.AddShapeToTopOfShapeMatrixAtPointerPosition();
        }
        UpdateBoardViewAccordingToBoard();
        yield return new WaitForSeconds(GameSpeed);
        int sumRowCompleted = m_Board.CollapseAllCompletedRowsAndScore();
        if (sumRowCompleted == 1)
            score += Board.ScoreOneRow;
        if (sumRowCompleted == 2)
            score += Board.ScoreTwoRows;
        if (sumRowCompleted == 3)
            score += Board.ScoreThreeRows;
        if (sumRowCompleted == 4)
            score += Board.ScoreFourRows;
        StartCoroutine(TimeTick());
    }

    public void ExecuteCommand(InputOptions options)
    {
        switch (options)
        {
            case InputOptions.Up:
                m_Board.TryMovePointer(1, 0);
                break;
            case InputOptions.Down:
                m_Board.TryMovePointer(-1, 0);
                break;
            case InputOptions.Left:
                m_Board.TryMovePointer(0, 1);
                break;
            case InputOptions.Right:
                m_Board.TryMovePointer(0, -1);
                break;
            case InputOptions.Rotate:
                m_Board.RotateShape();
                break;
            default:
                break;
        }
        UpdateBoardViewAccordingToBoard();
    }
}
