﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape
{
    public const int MAX_SHAPE_SIZE = 4;
    private Vector3Int [] m_ShapeCoords;

    public static List<Shape> ShapeTypes = new List<Shape>();
    
    static Shape()
    {
        ShapeTypes.Add(new Shape(Shape1));
        ShapeTypes.Add(new Shape(Shape2));
        ShapeTypes.Add(new Shape(Shape3));
        ShapeTypes.Add(new Shape(Shape4));
        ShapeTypes.Add(new Shape(Shape5));
        ShapeTypes.Add(new Shape(Shape6));
        ShapeTypes.Add(new Shape(Shape7));
        ShapeTypes.Add(new Shape(Shape8));
    }

    public static Shape GetRandomShape()
    {
        int rand = Random.Range(0, 8);
        return ShapeTypes[rand];
    }

    public Shape(Vector3Int[] i_Shape)
    {
        m_ShapeCoords = new Vector3Int[MAX_SHAPE_SIZE];

        i_Shape.CopyTo(m_ShapeCoords, 0);
        m_ShapeCoords[MAX_SHAPE_SIZE - 1] = Vector3Int.zero;
    }

    public void CopyShape(Shape i_Shape)
    {
        m_ShapeCoords[0] = Vector3Int.zero;
        i_Shape.m_ShapeCoords.CopyTo(m_ShapeCoords, 1);
    }

    public Vector3Int[] ShapeCoords
    {
        get => m_ShapeCoords;
        private set => m_ShapeCoords = value;
    }

    #region Shape Types
    // Pivot is always Vector3Int.Zero
    /*
    *
    *
    *
    *
    */
    public readonly static Vector3Int[] Shape1 = 
        { new Vector3Int(0, 1, 0), new Vector3Int(0, 2, 0), new Vector3Int(0, -1, 0)};

    /*
    *
    **
    *
    */
    public readonly static Vector3Int[] Shape2 =
        { new Vector3Int(0, 1, 0), new Vector3Int(0, -1, 0), new Vector3Int(1, 0, 0)};

    /*
    *
    *
    **
    */
    public readonly static Vector3Int[] Shape3 =
        { new Vector3Int(0, 1, 0), new Vector3Int(0, -1, 0), new Vector3Int(1, -1, 0)};
    /*
     *
     **
      *
    */
    public readonly static Vector3Int[] Shape4 =
        { new Vector3Int(-1, 0, 0), new Vector3Int(0, -1, 0), new Vector3Int(-1, 1, 0)};

    public readonly static Vector3Int[] Shape5 =
        { new Vector3Int(0, 1, 0), new Vector3Int(0, 0, -1), new Vector3Int(0, 1, -1)};

    public readonly static Vector3Int[] Shape6 =
        { new Vector3Int(0, -1, 0), new Vector3Int(0, 0, -1), new Vector3Int(0, 1, -1)};

    public readonly static Vector3Int[] Shape7 =
        { new Vector3Int(0, -1, 0), new Vector3Int(0, -1, 1), new Vector3Int(1, -1, 0)};
    /*
     **
     **    
    */
    public readonly static Vector3Int[] Shape8 =
        { new Vector3Int(1, 0, 0), new Vector3Int(1, -1, 0), new Vector3Int(0, -1, 0)};

    #endregion Shape Types

    public void RotateShapeAroundYAxis()
    {
        Vector3Int tempCube;

        for (int i = 0; i < m_ShapeCoords.Length; i++)
        {
            tempCube = m_ShapeCoords[i];
            m_ShapeCoords[i].x = tempCube.z;
            m_ShapeCoords[i].z = -tempCube.x;
        }
    }

    public void RotateShapeAroundZAxis()
    {
        Vector3Int tempCube;

        for (int i = 0; i < m_ShapeCoords.Length; i++)
        {
            tempCube = m_ShapeCoords[i];
            m_ShapeCoords[i].x = tempCube.y;
            m_ShapeCoords[i].y = -tempCube.x;
        }
    }
}
