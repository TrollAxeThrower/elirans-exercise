﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Board
{
    #region Constants
    public static int matDimX = 7;
    public static int matDimY = 10;
    public static int matDimZ = 7;

    public static int ScoreOneRow = 40;
    public static int ScoreTwoRows = 100;
    public static int ScoreThreeRows = 300;
    public static int ScoreFourRows = 1200;
    #endregion

    #region Members
    private bool[,,] m_BoardMatrix = new bool[matDimX, matDimY, matDimZ];
    private bool[,,] m_ShapeMatrix = new bool[matDimX, matDimY, matDimZ];

    private Vector3Int m_Pointer = new Vector3Int(matDimX / 2, matDimY, matDimZ / 2);
    private Vector3Int[] m_CurrentShapeCoords = new Vector3Int[4];
    private Shape currentShape;
    #endregion 

    public void ResetBoard()
    {
        currentShape = new Shape(Shape.GetRandomShape().ShapeCoords);

        rotateAroundYaxis = false;
        for (int x = 0; x < matDimX; x++)
        {
            for (int z = 0; z < matDimZ; z++)
            {
                for (int y = 0; y < matDimY; y++)
                {
                    m_BoardMatrix[x, y, z] = false;
                }
            }
        }
    }

    public bool TurnTick()
    {
        return CurrentShapeOneRowDownReturnTrueIfLanded();
    }

    private bool CurrentShapeOneRowDownReturnTrueIfLanded()
    {
        bool isShapeLanded = CheckIfShapeLanded();

        if (isShapeLanded)
        {
            MoveShapeMatrixToBoardMatrixClearShapeMatrix();
        }
        else //!isShapeLanded
        {
            CurrentShapeOneRowDownUpdateShapeMatrix();
        }
        return isShapeLanded;
    }
    private void CurrentShapeOneRowDownUpdateShapeMatrix()
    {
        removeShapeCoordsFromShapeMatrix();

        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            m_CurrentShapeCoords[i].y--;
            m_ShapeMatrix
                [m_CurrentShapeCoords[i].x,
                m_CurrentShapeCoords[i].y,
                m_CurrentShapeCoords[i].z] = true;
        }
        setPointerCoordsAccordingToShapeCoords();
    }
    private bool CheckIfShapeLanded()
    {
        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            // See if any of current shape cubes is ground level
            if (m_CurrentShapeCoords[i].y == 0)
                return true;

            // Check if any of current shape cubes is 
            // directly above any of the board cubes
            if (m_BoardMatrix
                [m_CurrentShapeCoords[i].x,
                m_CurrentShapeCoords[i].y - 1,
                m_CurrentShapeCoords[i].z])
                return true;
        }

        return false;
    }

    private void MoveShapeMatrixToBoardMatrixClearShapeMatrix()
    {
        for (int x = 0; x < matDimX; x++)
            for (int z = 0; z < matDimZ; z++)
                for (int y = 0; y < matDimY; y++)
                {
                    if (m_ShapeMatrix[x, y, z])
                    {
                        // Copy shape matrix to board matrix
                        m_BoardMatrix[x, y, z] = true;
                        // Remove from shape matrix. It will be empty after
                        // theses loops
                        m_ShapeMatrix[x, y, z] = false;
                    }
                }
    }

    public delegate void DoActionOnCoords(int x, int y, int z);

    public void DoActionForEachCubeInMatrix
        (DoActionOnCoords doIfTrue, DoActionOnCoords doIfFalse)
    {
        for (int x = 0; x < matDimX; x++)
            for (int z = 0; z < matDimZ; z++)
                for (int y = 0; y < matDimY; y++)
                {
                    if (m_BoardMatrix[x, y, z] || m_ShapeMatrix[x, y, z])
                    {
                        doIfTrue.Invoke(x, y, z);
                    }
                    else
                    {
                        doIfFalse.Invoke(x, y, z);
                    }
                }
    }
    

    private bool rotateAroundYaxis;
    public void RotateShape()
    {
        removeShapeCoordsFromShapeMatrix();
        if (rotateAroundYaxis)
            currentShape.RotateShapeAroundYAxis();
        else
            currentShape.RotateShapeAroundZAxis();
        rotateAroundYaxis = !rotateAroundYaxis;

        calculateShapeCoords();
        moveShapeCoordsIntoBoardBounds();
        addShapeCoordsToShapeMatrix();
    }

    public void AddShapeToTopOfShapeMatrixAtPointerPosition()
    {
        Shape.GetRandomShape().ShapeCoords.CopyTo(currentShape.ShapeCoords,0);

        calculateNewShapeCoords();
        moveShapeCoordsIntoBoardBounds();
        // Sometimes pointer is moved because of collisions with the game board edges.
        setPointerCoordsAccordingToShapeCoords();
        addShapeCoordsToShapeMatrix();
    }

    private void setPointerCoordsAccordingToShapeCoords()
    {
        m_Pointer = m_CurrentShapeCoords[0]; //first element is always pivot
    }

    private void calculateNewShapeCoords()
    {
        for (int i = 0; i < currentShape.ShapeCoords.Length; i++)
        {
            m_CurrentShapeCoords[i].x = 
                m_Pointer.x   + currentShape.ShapeCoords[i].x;
            m_CurrentShapeCoords[i].y =  // put shape at the top of the board
                matDimY - 1     + currentShape.ShapeCoords[i].y;
            m_CurrentShapeCoords[i].z = 
                m_Pointer.z   + currentShape.ShapeCoords[i].z;
        }
    }

    private void calculateShapeCoords()
    {
        for (int i = 0; i < currentShape.ShapeCoords.Length; i++)
        {
            m_CurrentShapeCoords[i].x =
                m_Pointer.x + currentShape.ShapeCoords[i].x;
            m_CurrentShapeCoords[i].y =  // put shape at the top of the board
                m_Pointer.y + currentShape.ShapeCoords[i].y;
            m_CurrentShapeCoords[i].z =
                m_Pointer.z + currentShape.ShapeCoords[i].z;
        }
    }

    private void moveShapeCoordsIntoBoardBounds()
    {
        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            while (m_CurrentShapeCoords[i].x < 0)
                moveShapeCoords(1, 0, 0);
            while (m_CurrentShapeCoords[i].x >= matDimX)
                moveShapeCoords(-1, 0, 0);
            while(m_CurrentShapeCoords[i].y < 0)
                moveShapeCoords(0, 1, 0);
            while (m_CurrentShapeCoords[i].y >= matDimY)
                moveShapeCoords(0, -1, 0);
            while (m_CurrentShapeCoords[i].z < 0)
                moveShapeCoords(0, 0, 1);
            while (m_CurrentShapeCoords[i].z >= matDimZ)
                moveShapeCoords(0, 0, -1);
        }
    }

    private void moveShapeCoords(int x, int y, int z)
    {
        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            m_CurrentShapeCoords[i].x += x;
            m_CurrentShapeCoords[i].y += y;
            m_CurrentShapeCoords[i].z += z;
        }
    }

    private void removeShapeCoordsFromShapeMatrix()
    {
        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            m_ShapeMatrix
                   [m_CurrentShapeCoords[i].x,
                    m_CurrentShapeCoords[i].y,
                    m_CurrentShapeCoords[i].z] = false;
        }
    }

    private void addShapeCoordsToShapeMatrix()
    {
        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            m_ShapeMatrix
                   [m_CurrentShapeCoords[i].x,
                    m_CurrentShapeCoords[i].y,
                    m_CurrentShapeCoords[i].z] = true;
        }
    }
    
    public void TryMovePointer(int x, int z)
    {
        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            // Check whether moving the shape makes it go out of bounds.
            if ((m_CurrentShapeCoords[i].x + x < 0)
             || (m_CurrentShapeCoords[i].x + x >= matDimX)
             || (m_CurrentShapeCoords[i].z + z < 0)
             || (m_CurrentShapeCoords[i].z + z >= matDimZ))
            {
                Debug.Log("Out of bounds of board");
                return;
            }

            //Check collision with existing squares in board 
            if(m_BoardMatrix
                [m_CurrentShapeCoords[i].x + x,
                 m_CurrentShapeCoords[i].y,
                 m_CurrentShapeCoords[i].z + z])
            {
                Debug.Log("Collision");
                return;
            }
        }

        removeShapeCoordsFromShapeMatrix();

        // Move the pointer and the shape if it's ok to move.
        m_Pointer.x += x;
        m_Pointer.z += z;

        for (int i = 0; i < m_CurrentShapeCoords.Length; i++)
        {
            m_CurrentShapeCoords[i].x += x;
            m_CurrentShapeCoords[i].z += z;
        }

        addShapeCoordsToShapeMatrix();
    }

    public bool IsGameOver()
    {
        for (int x = 0; x < matDimX; x++)
        {
            for (int z = 0; z < matDimZ; z++)
            {
                if (m_BoardMatrix[x, matDimY - 1, z] == true)
                    return true;
            }
        }
        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns> Amount of rows completed</returns>
    public int CollapseAllCompletedRowsAndScore()
    {
        int sumRowCompleted = 0;
        for (int y = 0; y < matDimY; y++)
        {
            if (isRowComplete(y))
            {
                sumRowCompleted++;
                RemoveRow(y);
            }
        }
        
        return sumRowCompleted;
    }

    private bool isRowComplete(int row)
    {
        for (int x = 0; x < matDimX; x++)
        {
            for (int z = 0; z < matDimZ; z++)
            {
                if (m_BoardMatrix[x, row, z] == false)
                    return false;
            }
        }

        return true;
    }

    // Removes all tiles from a row, and collapse all tiles above that row
    // one tile down.
    private void RemoveRow(int row)
    {
        for (int x = 0; x < matDimX; x++)
        {
            for (int z = 0; z < matDimZ; z++)
            {
                CollapseColumn(x, row, z);
            }
        }
    }

    // Moves all bricks higher than row one tile down.
    private void CollapseColumn(int x, int z, int row)
    {
        for (int y = row; y < matDimY - 1; y++)
        {
            m_BoardMatrix[x, y, z] = m_BoardMatrix[x, y + 1, z];
        }
    }
}
