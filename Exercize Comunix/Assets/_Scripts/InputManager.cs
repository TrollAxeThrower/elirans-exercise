﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public GameState gameState { get; set; }

    [SerializeField]
    private BoardView m_BoardView;

    [SerializeField]
    private KeyCode upKey = KeyCode.UpArrow;
    [SerializeField]
    private KeyCode downKey = KeyCode.DownArrow;
    [SerializeField]
    private KeyCode leftKey = KeyCode.LeftArrow;
    [SerializeField]
    private KeyCode rightKey = KeyCode.RightArrow;
    [SerializeField]
    private KeyCode rotateKey = KeyCode.KeypadEnter;

    public enum GameState
    {
        AllowInput,
        DontAllowInput
    }

    // Start is called before the first frame update
    void Start()
    {
        //gameState = GameState.AllowInput;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameState == GameState.AllowInput)
            getCommandsForOnePlayer();
    }

    private void getCommandsForOnePlayer()
    {
        if (Input.GetKeyUp(upKey))
            m_BoardView.ExecuteCommand(BoardView.InputOptions.Up);
        if (Input.GetKeyUp(downKey))
            m_BoardView.ExecuteCommand(BoardView.InputOptions.Down);
        if (Input.GetKeyUp(leftKey))
            m_BoardView.ExecuteCommand(BoardView.InputOptions.Left);
        if (Input.GetKeyUp(rightKey))
            m_BoardView.ExecuteCommand(BoardView.InputOptions.Right);
        if (Input.GetKeyUp(rotateKey))
            m_BoardView.ExecuteCommand(BoardView.InputOptions.Rotate);
    }
}
