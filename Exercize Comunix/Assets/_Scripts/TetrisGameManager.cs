﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisGameManager : MonoBehaviour
{
    public BoardView Player1Board;
    public BoardView Player2Board;

    public InputManager Player1Input;
    public InputManager Player2Input;

    [SerializeField]
    private float GameSpeed = 1f;
    [SerializeField]
    private float GameSpeedFactor = 1.2f;

    // Start is called before the first frame update
    void Start()
    {
        ActivatePlayer(Player1Board);
        ActivatePlayer(Player2Board);

        InitializeInput(Player1Input);
        InitializeInput(Player2Input);
    }

    private void ActivatePlayer(BoardView boardView)
    {
        boardView.CreateBoard();
        boardView.AddShape();
        boardView.UpdateBoardViewAccordingToBoard();
        boardView.GameSpeed = GameSpeed;
        boardView.GameSpeedFactor = GameSpeedFactor;
        boardView.StartGame();

    }

    private void InitializeInput(InputManager input)
    {
        input.gameState = InputManager.GameState.AllowInput;
    }
}
